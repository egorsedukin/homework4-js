const number1 = prompt("Enter first number");
const sign = prompt("Enter sign");
const number2 = prompt("Enter second number");

function operation(a, b, c) {
    let result;
    switch (b) {
        case '+':
            result = a + c;
            break;
        case '-':
            result = a - c;
            break;
        case '/':
            result = a / c;
            break;
        case '*':
            result = a * c;
            break;
        default:
            alert('Enter one of next signs: "+" "-" "*" "/"');
    }
    return (result);
}

console.log(operation(number1, sign, number2));